//
//  main.m
//  practica5
//
//  Created by Walos on 05/03/14.
//  Copyright (c) 2014 jesus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAAppDelegate class]));
    }
}
