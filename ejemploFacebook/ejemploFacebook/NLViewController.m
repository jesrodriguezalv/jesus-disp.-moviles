//
//  NLViewController.m
//  ejemploFacebook
//
//  Created by Patlan on 02/04/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import "NLViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)oprimir:(id)sender {
    SLComposeViewController *controladorSocial;
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        controladorSocial=[[SLComposeViewController alloc]init];
        controladorSocial=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controladorSocial setInitialText:@"sd"];
        [self presentViewController:controladorSocial animated:YES completion:nil];
    }
    [controladorSocial setCompletionHandler:^(SLComposeViewControllerResult result)
     {
         NSString *output;
         switch (result) {
             case SLComposeViewControllerResultCancelled:
                 output=@"Cancelado";
                 break;
            case SLComposeViewControllerResultDone:
                 output=@"trivia social posteada";
                 break;
             default:
                 break;
         }
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
@end
