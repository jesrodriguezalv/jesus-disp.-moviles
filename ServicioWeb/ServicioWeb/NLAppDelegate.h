//
//  NLAppDelegate.h
//  ServicioWeb
//
//  Created by Patlan on 03/04/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
