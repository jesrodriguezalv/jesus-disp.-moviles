//
//  NLAppDelegate.h
//  AlmacenamientoSecundario
//
//  Created by Patlan on 25/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
