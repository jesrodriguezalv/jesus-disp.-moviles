//
//  NLViewController.m
//  AlmacenamientoSecundario
//
//  Created by Patlan on 25/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController
-(NSString *)leerDatos
{
    NSString *directorio=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *rutaCompleta=[directorio stringByAppendingPathComponent:@"archivo"];
    NSStringEncoding codificacion=NSUTF8StringEncoding;
    NSError *error;
    NSString *textoLeido=[NSString stringWithContentsOfFile:rutaCompleta encoding:codificacion error:&error];
    if(textoLeido==nil)textoLeido=@"no inicializado";
    return textoLeido;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.texto.text=[self leerDatos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)terminaEdicion:(id)sender {
    [_texto resignFirstResponder];
}
@end
