//
//  NLViewController.h
//  ServicioJSON
//
//  Created by Patlan on 10/04/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)escribirCodigo:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *temperatura;
@property (strong, nonatomic) IBOutlet UILabel *presion;
@property (strong, nonatomic) IBOutlet UILabel *poblacion;

@end
