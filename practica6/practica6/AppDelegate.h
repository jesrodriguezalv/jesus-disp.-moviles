//
//  AppDelegate.h
//  practica6
//
//  Created by Patlan on 13/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
