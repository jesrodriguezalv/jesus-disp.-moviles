//
//  MOTableViewControllerAlumnos.h
//  practica6
//
//  Created by Patlan on 13/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MODetalleViewController.h"
@interface MOTableViewControllerAlumnos : UITableViewController
<AlumnoDetallesViewControllerDelegate>

@end
