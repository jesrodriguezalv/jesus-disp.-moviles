//
//  CAAppDelegate.h
//  ControlesAvanzados
//
//  Created by Walos on 05/03/14.
//  Copyright (c) 2014 jesus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
