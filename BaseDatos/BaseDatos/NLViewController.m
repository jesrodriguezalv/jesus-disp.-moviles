//
//  NLViewController.m
//  BaseDatos
//
//  Created by Patlan on 27/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import "NLViewController.h"
#import "NSObject+ProxyBD.m"
#import "NSObject+ProxyBD.h"
@interface NLViewController ()

@end

@implementation NLViewController
@synthesize proxyBD;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self copiaBD];
    self.proxyBD=[[NSObject alloc]init];
}
-(void)copiaBD{

    NSString *documentsPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString*bd=[documentsPath stringByAppendingPathComponent:@"mibd"];
    BOOL existeArchivo=[[NSFileManager defaultManager]fileExistsAtPath:bd];
    if(existeArchivo)return;
    NSString *rutaResource=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:@"mibd"];
    NSFileManager *administradorArchivos=[NSFileManager defaultManager];
    NSError *error=nil;
    if(![administradorArchivos copyItemAtPath:rutaResource toPath:bd error:&error])
    {
        UIAlertView *mensajeError=[[UIAlertView alloc]initWithTitle:@"!cuidado¡" message:@"no puedo copiar la bd" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [mensajeError show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)oprimir:(id)sender {
}
@end
