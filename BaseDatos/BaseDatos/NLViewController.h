//
//  NLViewController.h
//  BaseDatos
//
//  Created by Patlan on 27/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+ProxyBD.m"
#import "NSObject+ProxyBD.h"
@interface NLViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *texto;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
- (IBAction)oprimir:(id)sender;
@property (strong, nonatomic) NSObject *proxyBD;
@end
