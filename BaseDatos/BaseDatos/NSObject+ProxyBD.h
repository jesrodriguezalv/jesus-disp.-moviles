//
//  NSObject+ProxyBD.h
//  BaseDatos
//
//  Created by Patlan on 31/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ProxyBD)
-(NSMutableArray *)nombres;
-(void)insertarNombre:(NSString *)nombre;
@end
