//
//  NSObject+ProxyBD.m
//  BaseDatos
//
//  Created by Patlan on 31/03/14.
//  Copyright (c) 2014 Yizus. All rights reserved.
//

#import "NSObject+ProxyBD.h"
#include <sqlite3.h>
@implementation NSObject (ProxyBD)
-(NSMutableArray *)nombres
{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    const char *consulta="select nombre from informacion";
    NSString *ruta=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    sqlite3_open([ruta UTF8String], &laBd);
    sqlite3_prepare(laBd, consulta, -1, &consultaPreparada, NULL);
    NSMutableArray *resultados=[[NSMutableArray alloc]init];
    while(sqlite3_step(consultaPreparada)==SQLITE_ROW)
    {
        [resultados addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(consultaPreparada, 0)]];
    }
    sqlite3_close(laBd);
    return resultados;
    
}
-(void)insertarNombre:(NSString *)nombre
{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    const char *consulta="insert into informacion(nombre)values(?)";
    NSString *ruta=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    sqlite3_open([ruta UTF8String], &laBd);
    sqlite3_prepare(laBd, consulta, -1, &consultaPreparada, NULL);
    sqlite3_bind_text(consultaPreparada, 1, [nombre UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_step(consultaPreparada);
    sqlite3_finalize(consultaPreparada);
    sqlite3_close(laBd);
}

@end
